===============
Buscador de RUC
===============

SUNCOM RUC es un pequeño buscador para encontrar un RUC en el Registro Único del Contribuyente de Paraguay.

Última actualización: Febrero de 2013

	.. image:: img/ruc2.jpg

Tecnologias usadas
------------------

`Play Framework <http://playframework.org/>`_ 
	Playing Asynch
`Elastisearch <http://elasticsearch.com/"/>`_ 
	Ultra-fast Search Library and Server
`Twitter Bootstrap <http://twitter.github.com/bootstrap/base-css.html"/>`_ 
	Sleek, intuitive, and powerful front-end framework for faster and easier web development
`jQuery <http://jquery.com/>`_ 
	jQuery is a fast and concise JavaScript Library that simplifies HTML document traversing, event handling, animating, and Ajax interactions for rapid web development 
`Scala <http://www.scala-lang.org/>`_ 
	Scala is a general purpose programming language designed to express common programming patterns in a concise, elegant, and type-safe way 
	
El código fuente esta disponible bajo la licencia `MIT <http://mit-license.org/>`_ en `Bitbucket <https://bitbucket.org/softwarerero/ruc.sun.com.py>`_. 

Si usted quiere integrar este buscador en sus sistemas o usarlo como un webservice pongase en contacto con nosotros.
	
