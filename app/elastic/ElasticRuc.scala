package elastic

import org.elasticsearch.common.settings.ImmutableSettings
import org.elasticsearch.node.NodeBuilder

object ElasticRuc {

//  val elasticDir = RucConfig.config.elasticDir
  val elasticDir = play.api.Play.current.configuration.getString("elasticDir").getOrElse("data/elastic/rucs")
 
  val settings = ImmutableSettings.settingsBuilder()
    .put("node.name", "ruc")
    .put("path.data", elasticDir)
    .put("http.enabled", false)

  val node = NodeBuilder.nodeBuilder()
    .settings(settings)
    .clusterName("ruc-cluster")
    .data(true).local(true).node()

//  client.admin().cluster().prepareHealth("ruc").
//    setWaitForYellowStatus().execute().actionGet()

  val client = node.client()

  def shutdown() = {
    node.close()
    client.close()
  }
  
}