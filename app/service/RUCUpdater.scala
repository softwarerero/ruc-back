package service

import java.net.URL
import play.api.Logger
import java.io.File
import scala.Predef.Map.apply

trait RUCUpdater {

  val rucUpdatePath = play.api.Play.current.configuration.getString("rucUpdatePath").getOrElse("/tmp/")
  val rucFilePath = play.api.Play.current.configuration.getString("rucFileName").getOrElse("rucAll.rar")
  val setUrl = play.api.Play.current.configuration.getString("setUrl").get

  def downloadRucs() = {
    import java.io.{ File, FileInputStream, FileOutputStream }

    Logger.info("downloadRucs")
    val link = new URL(setUrl)
    val file = new File(rucUpdatePath + rucFilePath)

    org.apache.commons.io.FileUtils.copyURLToFile(link, file)

    Logger.info("file: " + file.getAbsolutePath())
    Logger.info("name: " + file.getName())
    Logger.info("length: " + file.length())
  }

  def unzip() = {
//    val cmd = "unrar e " + rucUpdatePath + rucFilePath + " -d " + rucUpdatePath
    val cmd = "unzip " + rucUpdatePath + rucFilePath + " -d " + rucUpdatePath
    Logger.info(cmd)
    val p = Runtime.getRuntime exec cmd
    for (i <- 0 to 9) {
      val cmd = "unzip " + rucUpdatePath + "ruc" + i + ".zip -d " + rucUpdatePath
      Logger.info(cmd)
      Runtime.getRuntime exec cmd
    }
  }

  def notFound() = Map("error" -> "Nada encontrado")

  def deleteRucs() = {}
  def processRucfiles() = {}
  def update()
  def findRuc(ruc: String): (String, Long, Long)
  def findContribuyente(name: String): (String, Long, Long)

  def count: Long
}