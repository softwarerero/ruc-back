import java.util.Locale
import play.api.GlobalSettings
import play.api.Logger
import elastic.ElasticRuc
import play.api.Application
import elastic.RUCUpdater


object Global extends GlobalSettings {

  override def onStart(app: Application) { 
    Logger.info("onStart")
    Locale.setDefault(new Locale("es", "PY"))
    
//    val updateActor = Akka.system.actorOf(Props[UpdateActor], name = "update")
//    Akka.system.scheduler.schedule(Duration(60, SECONDS), Duration(24, HOURS), updateActor, "update")
//    val updater = new RUCUpdater
//    updater.update
  }

  override def onStop(app: Application) {
    Logger.info("Shutdown Hook - cleaning up!");
    ElasticRuc.shutdown()
  }

}