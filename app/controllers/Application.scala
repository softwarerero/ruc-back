package controllers

import play.api._
import play.api.mvc._
import play.libs.Json
import elastic.Tracer

object Application extends Controller with XHR {

  val searchImplementationName = play.api.Play.current.configuration.getString("searchImplementation").getOrElse("elastic.RUCUpdater")
  val clazz = Class.forName(searchImplementationName)
  Logger.info("instantiate search clazz: " + clazz)
  val indexer = clazz.newInstance().asInstanceOf[service.RUCUpdater]

  def findRuc(ruc: String) = XhrAction { implicit request =>
    //        Logger.info("findRuc: " + ruc)
    Logger.info("findRuc: " + ruc + " : " + request.remoteAddress)
    val res = if (ruc.head.isDigit) {
      val index = if (ruc.indexOf("-") == -1) ruc.length() else ruc.indexOf("-")
      indexer.findRuc(ruc.substring(0, index))
    } else {
      indexer.findContribuyente(ruc)
    }
//    Logger.info("res: " + res._2)
    logQuery(ruc, res._2, request.remoteAddress)
    //    logAccess(request, res)
    Ok(res._1).as(JSON)
  }

  def logQuery(ruc: String, found: Long, ip: String) = {
    withPrintWriter(new java.io.File("logs/queries.log")) {
      writer => writer.println(ruc + ";" + found + ";" + ip)
    }

    import java.io.PrintWriter
    def withPrintWriter(file: java.io.File)(op: PrintWriter => Unit) {
      val writer = new PrintWriter(file)
      try {
        op(writer)
      } finally {
        writer.close()
      }
    }

  }

  def logAccess(request: Request[AnyContent], res: (String, Long, Long)) = {
    //  def logAccess(request: Request[AnyContent], ruc: String, found: Long, responseTime: Long) = {
    Logger.info("logAccess")

    Tracer.trace(res._1, res._2, res._3, request.remoteAddress)
  }

  def update = Action {
    try {
      //      BadRequest("no no")
      indexer.update()
      Ok("ok")
    } catch {
      case e: Exception => {
        Logger.error(e.toString())
        //        respondText(e.getMessage())
        e.printStackTrace()
        BadRequest(e.getMessage())
      }
    }
  }

  /**
   * Generates an `Action` that serves a static resource from an external folder
   *
   * @param absoluteRootPath the root folder for searching the static resource files such as `"/home/peter/public"`, `C:\external` or `relativeToYourApp`
   * @param file the file part extracted from the URL
   */
  def at(rootPath: String, file: String): Action[AnyContent] = Action { request =>
    import Play.current
    import java.io.File

    val AbsolutePath = """^(/|[a-zA-Z]:\\).*""".r

    val fileToServe = rootPath match {
      case AbsolutePath(_) => new File(rootPath, file)
      case _ => new File(rootPath + "/" + file)
    }
    Logger.debug("fileToServe: " + fileToServe.getAbsolutePath())

    if (fileToServe.exists) {
      Ok.sendFile(fileToServe, inline = true).withHeaders(CACHE_CONTROL -> "max-age=3600")
    } else {
      NotFound
    }

  }

}