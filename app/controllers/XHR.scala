package controllers

import play.api._
import play.api.mvc._

trait XHR extends Controller {

  val allowedOrigin = "http://localhost:9000"

  def options(url: String) = Action {
    Ok("").withHeaders(
      "Access-Control-Allow-Origin" -> "*",
//            "Access-Control-Allow-Origin" -> allowedOrigin,
      "Access-Control-Allow-Methods" -> "GET, POST, PUT, DELETE, OPTIONS",
      "Access-Control-Allow-Headers" -> "Content-Type, X-Requested-With, Accept",
      // cache access control response for one day
      //      "Access-Control-Max-Age" -> (60 * 60 * 24).toString)
      "Access-Control-Max-Age" -> (60).toString)
  }


  def XhrAction(f: Request[AnyContent] => Result): Action[AnyContent] = {
    Action { request =>
      Logger.info("XhrAction action: " + request)
      f(request).withHeaders("Access-Control-Allow-Origin" -> allowedOrigin)
    }
  }

}