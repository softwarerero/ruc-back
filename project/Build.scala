import sbt._
import Keys._
import play.Project._

object ApplicationBuild extends Build {

  val appName         = "rucback"
  val appVersion      = "1.0-SNAPSHOT"

  val appDependencies = Seq(
    // Add your project dependencies here,
    jdbc,
    anorm,
    "commons-io" % "commons-io" % "2.4",
    "org.elasticsearch" % "elasticsearch" % "0.19.11"
  )

// libraryDependencies += 
//libraryDependencies += "org.fusesource.scalamd" % "scalamd" % "1.5"


  val main = play.Project(appName, appVersion, appDependencies).settings(
    // Add your own project settings here      
  )

}
